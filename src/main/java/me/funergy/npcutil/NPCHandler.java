package me.funergy.npcutil;

import com.mojang.authlib.GameProfile;
import me.funergy.npcutil.nms.CustomPlayer;
import net.minecraft.server.v1_8_R1.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Funergy on 15/03/15.
 */
public class NPCHandler {
    private static NPCHandler instance;
    private HashMap<UUID,GameProfile> skinCache = new HashMap<UUID,GameProfile>();
    //We are caching the skins since the mojang has a max requests error.
    private ArrayList<Player> npcList = new ArrayList<>();

    public NPCHandler(){

    }

    public static NPCHandler getInstance(){
        if(instance == null){
            instance = new NPCHandler();
            return instance;
        }
        return instance;
    }

    public void spawnNPC(String name,UUID skin,Location loc) {
        GameProfile gP;
        if (skinCache.containsKey(skin)) {
            gP = skinCache.get(skin);
        } else {
            gP = new GameProfile(skin, name);
            skinCache.put(skin,gP);
        }
        CustomPlayer player = new CustomPlayer(skin, name, loc, gP);
        WorldServer world = ((CraftWorld) loc.getWorld()).getHandle();
        world.addEntity(player);
        npcList.add(player.getBukkitEntity());

    }

    public void removeNPC(Player p){
        p.remove();
    }

    public void showNPC(Player npc,Player player){
        if(npcList.contains(npc)){
            EntityPlayer nmsNpc = ((CraftPlayer)npc).getHandle();
            PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, nmsNpc);
            sendPacketTo(player, packet);

        }else{
            System.out.println("[NPCUtil] that player is not an npc!");
            return;
        }
    }
    public void hideNPC(Player p, Player npc) {
        EntityPlayer nmsNpc = ((CraftPlayer)npc).getHandle();
        PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, nmsNpc);
        sendPacketTo(p, packet);
    }
    private void sendPacketTo(Player p, Packet packet) {
        EntityPlayer recipient = ((CraftPlayer)p).getHandle();
        recipient.playerConnection.sendPacket(packet);
    }
}
