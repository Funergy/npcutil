package me.funergy.npcutil.nms;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_8_R1.EntityPlayer;
import net.minecraft.server.v1_8_R1.EnumGamemode;
import net.minecraft.server.v1_8_R1.PlayerInteractManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R1.CraftServer;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;

import java.util.UUID;

/**
 * Created by Funergy on 15/03/15.
 */
public class CustomPlayer extends EntityPlayer {

        public CustomPlayer(UUID uuid, String name, Location location,GameProfile prf) {
            super(((CraftServer)Bukkit.getServer()).getServer(), ((CraftWorld)location.getWorld()).getHandle(), makeProfile(name, uuid,prf), new PlayerInteractManager(((CraftWorld)location.getWorld()).getHandle()));
            playerInteractManager.b(EnumGamemode.SURVIVAL);
            playerConnection = new NPCConnection(this);

            setPosition(location.getX(), location.getY(), location.getZ());
            setYawPitch(location.getYaw(),location.getPitch());
        }

        public static GameProfile makeProfile(String name, UUID skinId,GameProfile skin) {
            GameProfile profile = new GameProfile(UUID.randomUUID(), name);
            if (skinId != null) {
                if (skin.getProperties().get("textures") == null || !skin.getProperties().get("textures").isEmpty()) {
                    Property textures = skin.getProperties().get("textures").iterator().next();
                    profile.getProperties().put("textures", textures);
                }
            }
            return profile;
        }
}