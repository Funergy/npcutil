package me.funergy.npcutil.nms;

import net.minecraft.server.v1_8_R1.EnumProtocolDirection;
import net.minecraft.server.v1_8_R1.NetworkManager;

import java.lang.reflect.Field;

/**
 * Created by Funergy on 15/03/15.
 */
public class NPCNetworkManager extends NetworkManager {

    public NPCNetworkManager() {
        super(EnumProtocolDirection.CLIENTBOUND);
        Field channel = makeField(NetworkManager.class, "i");
        Field address = makeField(NetworkManager.class, "j");

        setField(channel, this, new NullChannel());
        setField(address, this, new NullSocketAddress());

    }

    public static Field makeField(Class<?> clazz, String fieldName) {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static void setField(Field field, Object objToSet, Object value) {
        field.setAccessible(true);
        try {
            field.set(objToSet, value);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}