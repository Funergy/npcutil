package me.funergy.npcutil.nms;

import net.minecraft.server.v1_8_R1.Packet;
import net.minecraft.server.v1_8_R1.PlayerConnection;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R1.CraftServer;

/**
 * Created by Funergy on 15/03/15.
 */
public class NPCConnection extends PlayerConnection {

    public NPCConnection(CustomPlayer npc) {
        super(((CraftServer) Bukkit.getServer()).getServer(), new NPCNetworkManager(), npc);
    }
    @Override
    public void sendPacket(Packet packet) {
    }
}
